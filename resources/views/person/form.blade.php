<h1>This is the form</h1>
<form method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <div>
        <label for="name">Name</label>
        <input type="text" name="name" />
    </div>
    <div>
        <label for="nationality">Nationality</label>
        <input type="text" name="nationality" />
    </div>
    <div>
        <label for="date_of_birth">Date of births</label>
        <input type="text" name="date_of_birth" />
    </div>
    <div>
        <label for="place_of_birth">Place of birth</label>
        <input type="text" name="place_of_birth" />
    </div>
    <div>
        <label for="postcode">Postcode</label>
        <input type="text" name="postcode" />
    </div>
    <button type="submit">Send data!</button>
</form>
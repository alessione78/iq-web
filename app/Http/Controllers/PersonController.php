<?php

namespace App\Http\Controllers;

use App\Person;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    public function show(Request $request)
    {
        $people = Person::all();

        return view('hallo', array('persons' => $people));
    }

    public function add(Request $request)
    {
        if ($request->isMethod('POST')) {
            $person = new Person();
            $person->name = $request->get('name');
            $person->nationality = $request->get('nationality');
            $person->place_of_birth = $request->get('place_of_birth');
            $person->save();
        }

        return view('person.form');
    }
}
<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/person', 'PersonController@show');
Route::match(array('get', 'post'), '/person/add', 'PersonController@add');
